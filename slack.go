package tracing

import (
	"net/http"

	httptrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/net/http"
)

var (
	SlackHttpClient = httptrace.WrapClient(
		&http.Client{},
		httptrace.RTWithServiceName("slack-api"),
		httptrace.RTWithResourceNamer(slackAPIRTResourceNamer),
	)
)

func slackAPIRTResourceNamer(r *http.Request) string {
	return r.URL.Path
}
