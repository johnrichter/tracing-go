package tracing

import (
	"context"
	"io"
	"net/http"

	httptrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/net/http"
)

var (
	DefaultHTTPClient = httptrace.WrapClient(http.DefaultClient)
)

func Do(ctx context.Context, client *http.Client, method, url string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		return nil, err
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func DoSlackApi(ctx context.Context, method, url string, body io.Reader) (*http.Response, error) {
	return Do(ctx, SlackHttpClient, method, url, body)
}
